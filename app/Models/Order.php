<?php
declare(strict_types=1);


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class Order extends Model
{
    protected $fillable = ['order_session'];

    protected $table = 'order';

    public function orderItems(): HasMany
    {
        return $this->hasMany(OrderItems::class);
    }
}
