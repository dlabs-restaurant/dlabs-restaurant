<?php
declare(strict_types=1);


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

final class OrderItems extends Model
{
    protected $fillable = ['order_id', 'menu_id', 'is_prepared', 'is_delivered'];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function menu(): BelongsTo
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public static function createOrderItem(int $orderId, int $menuId): void
    {
        OrderItems::create([
            'order_id' => $orderId,
            'menu_id' => $menuId,
            'is_prepared' => false,
            'preparation_started' => false
        ]);
    }
}
