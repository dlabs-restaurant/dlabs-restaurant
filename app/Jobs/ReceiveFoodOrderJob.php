<?php
declare(strict_types=1);

namespace App\Jobs;

use App\Models\OrderItems;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ReceiveFoodOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $menuItemId;

    public function __construct(int $orderId, int $menuItemId)
    {
        $this->queue = 'kitchen';
        $this->orderId = $orderId;
        $this->menuItemId = $menuItemId;
    }

    public function handle(): void
    {
        OrderItems::createOrderItem($this->orderId, $this->menuItemId);
    }
}
