<?php
declare(strict_types=1);

namespace App\Jobs;

use App\Models\Order;
use App\Models\OrderItems;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckAllDrinkIsPreparedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $orderId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $orderId)
    {
        $this->queue = 'preparation';
        $this->orderId = $orderId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $items = OrderItems::join('menu', function($join): void {
            $join->on('order_items.menu_id', '=', 'menu.id')
                ->where('menu.type', 'drink');
        })->where('is_prepared' , false)
            ->where('order_id', $this->orderId)
            ->get();

        if(!$items->count()) {
            $order = Order::where('id', $this->orderId)->first();
            $order->is_drink_prepared = true;
            $order->save();
        }

    }
}
