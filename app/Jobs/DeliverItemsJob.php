<?php
declare(strict_types=1);

namespace App\Jobs;

use App\Models\Order;
use App\Models\OrderItems;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeliverItemsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var string
     */
    private $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $orderId, string $type)
    {
        $this->queue = 'delivery';
        $this->orderId = $orderId;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {

        try {
            $order = Order::where('id', $this->orderId)->first();
            if ($this->type == 'food') {
                $order->is_food_delivered = true;
            } else {
                $order->is_drink_delivered = true;
            }
            $order->save();

            $orderItems = OrderItems::select('order_items.*')
                ->join('menu', function($join): void {
                    $join->on('order_items.menu_id', '=', 'menu.id')
                        ->where('menu.type', $this->type);
                })->where('order_id', $this->orderId)->get();

            foreach ($orderItems as $item) {
                $item->is_delivered = true;
                $item->save();
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
