<?php
declare(strict_types=1);


namespace App\Http\Controllers;


use App\Models\Menu;
use App\Models\OrderItems;
use Illuminate\Support\Facades\Session;

final class MenuController extends Controller
{
    public function menu()
    {
        $menu = Menu::all();
        $orderedItems = $this->getOrderItems();
        $deliveredItems = $this->getDeliveredItems();

        return view('menu', \compact('menu', 'orderedItems', 'deliveredItems'));
    }

    private function getOrderItems()
    {
        $orderId = null;
        if (Session::has('orderId')) {
            $orderId = Session::get('orderId');
        }

        return OrderItems::where('order_id', $orderId)->with(['menu'])->orderBy('created_at')->get();
    }

    private function getDeliveredItems()
    {

        $orderId = null;
        if (Session::has('orderId')) {
            $orderId = Session::get('orderId');
        }

        return OrderItems::where('order_id', $orderId)
            ->with(['menu'])
            ->where('is_delivered', true)
            ->orderBy('created_at')->get();
    }
}
