<?php
declare(strict_types=1);


namespace App\Http\Controllers;


use App\Jobs\DeliverItemsJob;
use App\Models\Order;

final class WaiterController extends Controller
{
    public function dashboard()
    {
        $foodOrders = Order::select('order.*')
            ->join('order_items', function($join): void {
                $join->on('order_items.order_id', '=', 'order.id');
            })->join('menu', function($join): void {
                $join->on('order_items.menu_id', '=', 'menu.id')
                    ->where('menu.type', 'food');
            })->where('is_food_prepared', true)
            ->where('is_food_delivered', false)
            ->distinct()->get();

        $drinkOrders = Order::select('order.*')
            ->join('order_items', function($join): void {
                $join->on('order_items.order_id', '=', 'order.id');
            })->join('menu', function($join): void {
                $join->on('order_items.menu_id', '=', 'menu.id')
                    ->where('menu.type', 'drink');
            })->where('is_drink_prepared', true)
            ->where('is_drink_delivered', false)
            ->distinct()->get();

        return view('waiter', \compact('foodOrders', 'drinkOrders'));
    }

    public function markAsDelivered(int $orderId, string $type): bool
    {
        dispatch(new DeliverItemsJob($orderId, $type));

        return true;
    }
}
