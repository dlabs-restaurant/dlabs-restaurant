<?php
declare(strict_types=1);


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

final class AuthController
{
    public function login()
    {
        return view('login');
    }

    public function loginUser(Request $request)
    {

        Session::put('name', $request->get('name'));
        Session::put('role', $request->get('role'));

        if($request->get('role') === 'guest') {
            return redirect(route('menu'));
        }
        if($request->get('role') === 'waiter') {
            return redirect(route('waiter.dashboard'));
        }
        if($request->get('role') === 'barman') {
            return redirect(route('bar.dashboard'));
        }
        if($request->get('role') === 'chef') {
            return redirect(route('kitchen.dashboard'));
        }
    }

    public function logout()
    {
        Session::forget('orderId');
        Session::forget('name');
        Session::forget('role');
        Session::flush();

        return redirect(route('login-form'));
    }
}
