<?php
declare(strict_types=1);


namespace App\Http\Controllers;


use App\Jobs\CheckAllDrinkIsPreparedJob;
use App\Models\Order;
use App\Models\OrderItems;

final class BarController extends Controller
{
    public function dashboard()
    {
        $orders = Order::select('order.*')
            ->join('order_items', function($join): void {
                $join->on('order_items.order_id', '=', 'order.id')
                    ->where('order_items.is_prepared', false);
            })->join('menu', function($join): void {
                $join->on('order_items.menu_id', '=', 'menu.id')
                    ->where('menu.type', 'drink');
            })->orderBy('order.created_at', 'asc')
            ->distinct()->get();

        return view('barman', \compact('orders'));
    }

    public function markAsPrepared(int $orderItem): bool
    {
        $orderItem = OrderItems::where('id', $orderItem)->first();
        $orderItem->is_prepared = true;
        $orderItem->save();

        dispatch(new CheckAllDrinkIsPreparedJob($orderItem->order_id));

        return true;
    }
}
