<?php
declare(strict_types=1);


namespace App\Http\Controllers;


use App\Jobs\ReceiveDrinkOrderJob;
use App\Jobs\ReceiveFoodOrderJob;
use App\Models\Menu;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

final class OrderController extends Controller
{
    public function saveOrderItem(?int $menuItem): bool
    {
        $orderId = Session::get('orderId');

        if (!Session::has('orderId')) {

            $order = Order::firstOrCreate(['order_session' => Str::random(32)]);
            $orderId = $order->id;

            Session::put('orderId', $orderId);
            Session::save();
        }

        $menuItem = Menu::where('id', $menuItem)->first();

        if ($menuItem->type === 'food') {
            $job = (new ReceiveFoodOrderJob($orderId, $menuItem->id));
        }
        if ($menuItem->type === 'drink') {
            $job = (new ReceiveDrinkOrderJob($orderId, $menuItem->id));
        }

        dispatch($job);

        return true;
    }
}
