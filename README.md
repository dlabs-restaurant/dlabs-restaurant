    1.) copy .ev.example to .env
    2.) `vagrant up`
    3.) composer install
    4.) cd restaurant
    5.) php artisan migrate --seed
    6.) php artisan queue:work --queue=bar,kitchen,preparation,delivery
    7.) on your local machine in hosts file provide record 
        192.168.5.7 restaurant.local
    8.) in browser open restaurant.local

