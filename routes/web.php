<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['as' => 'login-form-index', 'uses' => 'AuthController@login']);
Route::get('/login', ['as' => 'login-form', 'uses' => 'AuthController@login']);
Route::post('/login', ['as' => 'login', 'uses' => 'AuthController@loginUser']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
Route::get('/menu', ['as' => 'menu', 'uses' => 'MenuController@menu']);
Route::get('/order-item/{menuId}', ['as' => 'order', 'uses' => 'OrderController@saveOrderItem']);

Route::get('/kitchen/markAsPrepared/{orderItemId}', ['as' => 'kitchen.prepared', 'uses' => 'KitchenController@markAsPrepared']);
Route::get('/kitchen/dashboard', ['as' => 'kitchen.dashboard', 'uses' => 'KitchenController@dashboard']);

Route::get('/bar/markAsPrepared/{orderItemId}', ['as' => 'bar.prepared', 'uses' => 'BarController@markAsPrepared']);
Route::get('/bar/dashboard', ['as' => 'bar.dashboard', 'uses' => 'BarController@dashboard']);

Route::get('/waiter/markAsDeliveredFood/{orderItemId}/{type}', ['as' => 'waiter.delivered', 'uses' => 'WaiterController@markAsDelivered']);
Route::get('/waiter/dashboard', ['as' => 'waiter.dashboard', 'uses' => 'WaiterController@dashboard']);
