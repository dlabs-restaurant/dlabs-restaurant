@extends('layout.app')

@section('content')

<form method="POST" action="/login">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="role">Role</label>
                <select class="form-control" name="role">
                    @foreach(['guest', 'waiter', 'barman', 'chef'] as $role)
                        <option value="{{ $role }}">{{ $role }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <input type="submit" value="Login">
</form>
@endsection
