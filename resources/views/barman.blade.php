@extends('layout.app')

@section('content')
    <h2>Open orders</h2>
    @foreach($orders as $order)
        <table class="table">
            <tr>
                <th>Order {{ $order->id }}</th>
                <th></th>
            </tr>
            @foreach($order->orderItems as $item)
                <tr>
                    @if($item->menu->type === 'drink')
                        <td>{{ $item->menu->name }}</td>
                        <td id="_{{ $item->id }}">
                            @if(!$item->is_prepared)
                                <a href="#" class="prepareOrderItem btn btn-success" rel="{{ $item->id }}">{{ __('Done') }}</a>
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
        </table>
    @endforeach

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('.prepareOrderItem').click(function (e) {
                e.preventDefault();
                let menuId = $(this).attr('rel');
                $.ajax({
                    type: "GET",
                    url: "/bar/markAsPrepared/" + menuId
                });
                $('#_' + id).html('')
            });
            setTimeout(function () {
                location.reload();
            }, 3000);
        });
    </script>
@endsection
