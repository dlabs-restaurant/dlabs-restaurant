@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <h4>Food ready for deliver</h4>
            @foreach($foodOrders as $order)
                <table class="table">
                    <tr>
                        <th>Order {{ $order->id }}, table: {{ $order->order_session }}</th>
                        <th><a href="#" class="deliver-food btn btn-success" rel="{{ $order->id }}">{{ __('Deliver') }}</a></th>
                    </tr>
                    @foreach($order->orderItems as $item)
                        <tr>
                            @if($item->menu->type === 'food')
                                <td>{{ $item->menu->name }}</td>
                            @endif
                        </tr>
                    @endforeach
                </table>
            @endforeach
        </div>
        <div class="col-sm-6">
            <h4>Drink ready for deliver</h4>
            @foreach($drinkOrders as $order)
                <table class="table">
                    <tr>
                        <th>Order {{ $order->id }}, table: {{ $order->order_session }}</th>
                        <th><a href="#" class="deliver-drink btn btn-success" rel="{{ $order->id }}">{{ __('Deliver') }}</a></th>
                    </tr>
                    @foreach($order->orderItems as $item)
                        <tr>
                            @if($item->menu->type === 'drink')
                                <td>{{ $item->menu->name }}</td>
                            @endif
                        </tr>
                    @endforeach
                </table>
            @endforeach
        </div>
    </div>


@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('.deliver-food').click(function (e) {
                e.preventDefault();
                let orderId = $(this).attr('rel');
                $.ajax({
                    type: "GET",
                    url: "/waiter/markAsDeliveredFood/" + orderId + '/food'
                });
                $('#_' + id).html('')
            });
            $('.deliver-drink').click(function (e) {
                e.preventDefault();
                let orderId = $(this).attr('rel');
                $.ajax({
                    type: "GET",
                    url: "/waiter/markAsDeliveredFood/" + orderId + '/drink'
                });
            });
            setTimeout(function () {
                location.reload();
            }, 3000);
        });
    </script>
@endsection
