@extends('layout.app')

@section('content')
    <h2>Today's menu</h2>
    <table class="table">
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Type</th>
            <th></th>
        </tr>
        @foreach($menu as $menuItem)
            <tr>
                <td>{{ $menuItem->name }}</td>
                <td>{{ $menuItem->type }}</td>
                <td>{{ $menuItem->price }}</td>
                <td><a href="#" class="doOrder btn btn-success" rel="{{ $menuItem->id }}">{{ __('Order') }}</a></td>
            </tr>
        @endforeach
    </table>
    <br/>
    @if($orderedItems->count())
        <div class="row">
            <div class="col-sm-6">
                <h3>Your items in order: {{ $orderedItems[0]->order_id }}</h3>
                <table id="tableOrder" class="table">
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                    <tbody>
                    @foreach($orderedItems as $item)
                        <tr>
                            <td>{{ $item->menu->name }} ({{ $item->menu->type }})</td>
                            <td>{{ $item->menu->price }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-sm-6">
                <h3>Your delivered items</h3>
                <table id="tableOrder" class="table">
                    <tr>
                        <th>Name</th>
                    </tr>
                    <tbody>
                    @foreach($deliveredItems as $item)
                        <tr>
                            <td>{{ $item->menu->name }} ({{ $item->menu->type }})</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('.doOrder').click(function (e) {
                e.preventDefault();
                let menuId = $(this).attr('rel');
                $.ajax({
                    type: "GET",
                    url: "/order-item/" + menuId
                });
            });
            setTimeout(function () {
                location.reload();
            }, 3000);
        });
    </script>
@endsection
