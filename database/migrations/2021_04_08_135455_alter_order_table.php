<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('order', function(Blueprint $table): void {
            $table->boolean('is_food_prepared')->default(false)->after('order_session');
            $table->boolean('is_drink_prepared')->default(false)->after('is_food_prepared');
            $table->boolean('is_food_delivered')->default(false)->after('is_drink_prepared');
            $table->boolean('is_drink_delivered')->default(false)->after('is_food_delivered');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('order', function(Blueprint $table): void {
            $table->dropColumn('is_food_prepared');
            $table->dropColumn('is_drink_prepared');
            $table->dropColumn('is_food_delivered');
            $table->dropColumn('is_drink_delivered');
        });
    }
}
