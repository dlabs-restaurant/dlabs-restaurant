<?php
declare(strict_types=1);


namespace Database\Seeders;


use App\Models\Menu;
use Illuminate\Database\Seeder;

final class MenuSeeder extends Seeder
{
    public function run(): void
    {
        $menuItems = [
            [
                'name' => 'Grilled space-whale steak with algae puree',
                'type' => 'food',
                'price' => 66.5
            ],
            [
                'name' => 'Tea substitute ',
                'type' => 'drink',
                'price' => 1.5
            ],
            [
                'name' => 'Hagro biscuit',
                'type' => 'food',
                'price' => 32.00
            ],
            [
                'name' => 'Ameglian Major Cow casserole',
                'type' => 'food',
                'price' => 55.75
            ],
            [
                'name' => 'Pan Galactic Gargle Blaster',
                'type' => 'drink',
                'price' => 5.5
            ],
            [
                'name' => 'Janx Spirit',
                'type' => 'drink',
                'price' => 7
            ],
            [
                'name' => 'Tzjin-anthony-ks the Gagrakackan version of the gin and tonic.',
                'type' => 'drink',
                'price' => 11.5
            ],
        ];

        foreach($menuItems as $item) {
            Menu::updateOrCreate(
                $item,
                $item
            );
        }

    }
}
